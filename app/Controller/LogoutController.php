<?php

/*
 * All files and code are copyright of BOS and should not replicated without
 * expilicit authorization from the BOS executive in charge of this project. 
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP LogoutController
 * @author Anup Kale
 */
class LogoutController extends AppController {

    var $user = array(); 
    
    public function beforeFilter() {
        parent::beforeFilter();
         $this->Authenticate->allow();
    }
    
    /*
     * Function Logout will be used to login a user
     * This controller has been seggregated from the Users as we will be
     * definining all login related functions and methods here
     * 
     * @author Anup Kale
     * @created 07/15/2014.
     */
    public function index() {
        if(!$this->Authenticate->loggedIn())
        {
            return $this->redirect($this->Authenticate->logout());
        }
        else
        {
            $this->redirect($this->Authenticate->logout());
        }
    }

}
/*
 * EOF
 * LoginController.php
 * ./app/Controller/LoginController.php
 */
