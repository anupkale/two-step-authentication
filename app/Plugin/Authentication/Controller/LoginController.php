<?php
/**
 * Vanilla Authentication component of CakePHP(tm) uses basic form validation
 * where username and password challenge is thrown to the end user.
 * After the username and password is entered the user is Authroised and allowed
 * access to the system. 
 * 
 * Any code change to accept two step authentication does not really work as the 
 * user is already authenticated and isAuthorised() method becomes heavy and
 * looses it's true purpose.
 * 
 * We need an authentication system with which a developer did not loose
 * the comfort and familiarity of the Cake's implementation of Auth Component and
 * get the flexibility to work with two step process. 
 *
 * Manages user logins and permissions using Two Step Authentication.
 * 
 * @disclaimer This pluggin is provided as is and that Itexecutors Consultancy 
 * Services Pvt. Ltd, its subsidaries, partners and employees will not be liable 
 * for any loss incurred in any manner due to the direct or indirect use of this 
 * component.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org) and is a
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 *
 * @copyright     Itexecutors Consultancy Services Pvt. Ltd (http://www.itexecutors.com)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Authenticate.Controller.Component
 * @version       0.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP LoginController
 * @author Anup Kale <anup.kale@itexecutors.com>
 * @todo AJAX requested two step authentication is not yet tested.
 */
class LoginController extends AppController {
    
    var $uses = array();
    
    var $components =  array('RequestHandler');
    
    /**
     * Function Login will be used to login a user
     * This controller has been seggregated from the Users as we will be
     * definining all login related functions and methods here
     * 
     * @author Anup Kale
     * @created 07/24/2014.
     * @param NULL
     * @return Void Mehtod does not retrun value. It renders a view to the user.
     */
    public function index() {
        $this->Session->delete('ForgotPassword');
        
        if($this->Authenticate->loggedIn())
        {
            $this->Session->setFlash(__('Please! You are already logged in.'));
            return $this->redirect($this->referer());
        }
        if ($this->request->is('post') ) {
            if($this->Authenticate->login()) {
                return $this->redirect($this->Authenticate->redirectUrl());
            }
            else {
                $this->Session->setFlash('Invalid Username or Password!');
            }
        }
    }
    
    /**
     * Base function for developers to understand how the Authentication pluggin's
     * authenticateSecondStep() works.
     * The development and logic of this function is left to development team to
     * develop according to thier business rules.
     * 
     * @author Anup Kale
     * @created 07/24/2014.
     * @param NULL
     * @return Void Mehtod does not retrun value. It renders a view to the user.
     */
    public function secondStepAuthenticate()
    {
        if($this->request->is('post'))
        {
            switch($this->Authenticate->authenticateSecondStep($this->request->data['key'] == $this->Session->read('Code'))) {
                case true:
                    return $this->redirect($this->Authenticate->redirectUrl());
                case false:
                    $this->Session->setFlash('Invalid Key');
                    break;
                default:
                    return $this->redirect($this->Authenticate->loginAction);
            }
            
        }
        $code = substr(mt_rand(), 0, 6);
     
        $this->Session->write('Code', $code);
        
        $message = 'Your OTP for Second Step Authentication is: '.$code;
        $this->set(compact('message'));
    }

}
/*
 * EOF
 * LoginController.php
 * ./app/Plugin/Authentication/Controller/LoginController.php
 */