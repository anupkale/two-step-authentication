<?php
/**
 * Vanilla Authentication component of CakePHP(tm) uses basic form validation
 * where username and password challenge is thrown to the end user.
 * After the username and password is entered the user is Authroised and allowed
 * access to the system. 
 * 
 * Any code change to accept two step authentication does not really work as the 
 * user is already authenticated and isAuthorised() method becomes heavy and
 * looses it's true purpose.
 * 
 * We need an authentication system with which a developer did not loose
 * the comfort and familiarity of the Cake's implementation of Auth Component and
 * get the flexibility to work with two step process. 
 *
 * Manages user logins and permissions using Two Step Authentication.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see http://mit-license.org/
 * Redistributions of files must retain the above copyright notice.
 * 
 * @disclaimer This plugin is provided as is and that Itexecutors Consultancy 
 * Services Pvt. Ltd, its subsidaries, partners and employees will not be liable 
 * for any loss incurred in any manner due to the direct or indirect use of this 
 * component.
 * 
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org) and is a
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 *
 * @copyright     Itexecutors Consultancy Services Pvt. Ltd (http://www.itexecutors.com)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Authenticate.Controller.Component
 * @version       0.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/*
 * Include Cakephp Auth Component.
 */
App::uses('AuthComponent', 'Controller/Component');
/**
 * CakePHP Authenticate Component extends the following methods from Auth 
 * Component:
 * 1. _isLoginAction()
 * 2. login()
 * 3. logout()
 * 4. redirectUrl()
 * 
 * This component introduces new method
 * authenticateSecondStep()
 *  
 * @author Anup Kale <anup.kale@itexecutors.com>
 * @todo AJAX requested two step authentication is not yet tested.
 */
class AuthenticateComponent extends AuthComponent  {
    
    /**
     * Use the same components that Auth components use.
     */
    public $components = array('Session', 'RequestHandler');
    
    /**
     * Allows a development team to turn of Two Step Authentication.
     * Not sure why they would want to use this pluggin if they do not want 
     * two step authentication...
     * 
     * @var boolean
     */
    public $secondStepAuthentication = true;
    
    /**
     * This is the default field that will be checked in the model to determine
     * whether the user has set second step authentication.
     * 
     * @var string
     */
    public $checkField = 'second_step_authentication';
    
    /**
     * The session key name where the record of the current user is stored. 
     * Default key is "IntermidiateAuth.User".
     * 
     * @var string
     */
    public static $sessionSecondStepKey = 'IntermidiateAuth.User';
    
    /** 
     * Default login action of the authenticate component.
     * 
     * @var array
     */
    public $loginAction = array(
        'controller' => 'login',
        'action' =>  'index',
        'plugin' => 'authentication'
    );
    
    /** 
     * Default Second step login action of the authenticate component.
     * 
     * @var array
     */
    public $loginActionTwo = array(
        'controller' => 'login',
        'action' => 'secondStepAuthenticate',
        'plugin' => 'authentication'
    );
    
    /** 
     * Default logout action of the authenticate component.
     * 
     * @var array
     */
    public $logoutRedirect = array(
        'controller' => '/',
    );
    
    /**
     * Normalizes $loginAction and checks if current request URL is same as login
     * or as Second Step login.
     * The Second Step login action is only accessible if the session is
     * set with IntermidiateAuth.User key. This is done to make sure
     * that direct access to second step is denied without using the first
     * step.
     * @param Controller $controller A reference to the controller object.
     * @return bool True if current action is login action else false.
     */
    protected function _isLoginAction(Controller $controller) {
       
        $url = '';
        if (isset($controller->request->url)) {
                $url = $controller->request->url;
        }
        $url = Router::normalize($url);
        $loginAction = Router::normalize($this->loginAction);
        
        $loginActionSecondStepVerification = Router::normalize($this->loginActionTwo);
        
        if($loginAction === $url)  {
            return $loginAction === $url;
        }
        else if($loginActionSecondStepVerification === $url and
            $this->Session->read('IntermidiateAuth.User')) {
            return $loginActionSecondStepVerification === $url;
        }
        else {
            return false;
        }
    }
    
    /**
     * Log a user in.
     * Step 1 of login. The usage of this funtion is the same as Cake's Auth 
     * login().
     * If the `$user` variable is empty then the cake request is used to identify
     * the user.
     * 
     * If the Second Step Authentication is set to TRUE and user is found then
     * then we will check if the user has selected 2 step authentication process
     * if the user has selected then s/he will be navigated to the second
     * step login action or else will be forwarded to Cake's Auth login funciton
     * to log the user. 
     *
     * @param array $user Either an array of user data, or null to identify a user using the current request.
     * @return bool True on login success, false on failure
     */
    public function login($user = null)
    {
        $this->_setDefaults();

        if (empty($user)) {
            $user = $this->identify($this->request, $this->response);
        }
        if($this->secondStepAuthentication and $user)
        {
            if(!empty($user[$this->checkField])) {
                $this->Session->renew();
                $this->Session->write(self::$sessionSecondStepKey, $user);
                return true;
            }
        }
        return parent::login($user);
    }
    
    /**
     * Log a user in.
     * Step 2 of login. 
     * This function basically leaves the development and login of the second 
     * step login process to the developer and requirement. This function 
     * basically requires the developer to tell the Authenticate Componenet
     * whether an user has met the set criteria of the project 2nd step authentication
     * and wether to allow the user access to the system or not.
     *
     * @param boolean from developer defined function. 
     * @return bool True on login success, false on failure
     */
    
    public function authenticateSecondStep($isAuthenticated)
    {
        $user = $this->Session->read(self::$sessionSecondStepKey);
        $this->Session->delete(self::$sessionSecondStepKey);
        
        if($isAuthenticated)  {
           return parent::login($user);
        }
        return $isAuthenticated;
    }
    
    /**
     * This function makes sure that the a user with second step authentication
     * is first redirected to the Second Step Authentication Object before being 
     * redirected to the referer page.
     * 
     * If a referer page is found then we store it in the Inetrmidiate Session '
     * data and then redirect the user to the login action 2
     * If there intermidiate data has a url then redirect the user to that url
     * else
     * redirect the user to the url provide by Cake's Auth redirect().
     *
     * @param string|array $url Optional URL to write as the login redirect URL.
     * @return string Redirect URL
     */
    public function redirectUrl($url = null) {
        if($this->secondStepAuthentication and 
                !$this->Session->read('Auth.User') and 
                $this->Session->read('IntermidiateAuth.User')) {
            $this->Session->write('IntermidiateAuth.redirect', parent::redirectUrl());
            return $this->loginActionTwo;
        }
        else if($this->Session->read('IntermidiateAuth.redirect')) {
            $redir = $this->Session->read('IntermidiateAuth.redirect');
            $this->Session->delete('IntermidiateAuth.redirect');
            return $redir;
        }
        else {
            return parent::redirectUrl();
        }
    }
    
    /**
     * Log a user out.
     *
     * Deletes any remaing Intermidiate Session keys and calls the Cake's Auth
     * logout().
     *
     * @return string AuthComponent::$logoutRedirect
     * @see AuthComponent::$logoutRedirect
     */    
    public function logout() {
        $this->Session->delete('IntermidiateAuth.redirect');
        $this->Session->delete('IntermidiateAuth.User');
        return parent::logout();
    }

}
/*
 * EOF
 * AuthenticateComponent.php
 * ./app/Plugin/Authentication/Controller/Component/AuthenticateComponent.php
 */